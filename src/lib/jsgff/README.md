jsGFF
=====

This library currently lives as a distribution package as part of the Dragon Age
Origins Save Viewer. This package is considered legacy and needs to be rebuilt and
distributed through npm instead of being a javascript bundle inside this project.
